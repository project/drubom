<?php

namespace Drupal\drubom;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\State;
use Drupal\Core\Transliteration\PhpTransliteration;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Drubom exec manager class.
 */
class DrubomExecManager {

  /**
   * The execution timeout.
   *
   * @var int
   */
  const PROCESS_TIMEOUT = 300;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\State\State $state
   *   The drupal state.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Transliteration\PhpTransliteration $transliteration
   *   The transliteration service.
   * @param string $appRoot
   *   The app root.
   */
  public function __construct(
    private readonly State $state,
    private readonly FileSystemInterface $fileSystem,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly LoggerInterface $logger,
    private readonly PhpTransliteration $transliteration,
    private readonly string $appRoot,
  ) {
  }

  /**
   * Validate the executable.
   *
   * @param string $binary
   *   The binary to validate.
   *
   * @return bool
   *   TRUE if the binary is valid.
   *
   * @throws \RuntimeException
   *   If the binary is not valid.
   */
  public function validateExecutable(string $binary) {
    if (empty($binary)) {
      throw new \RuntimeException("Binary cannot be empty.");
    }
    if (!is_file($binary)) {
      throw new \RuntimeException(strtr("%binary not found.", ['%binary' => $binary]));
    }
    if (!is_executable($binary)) {
      throw new \RuntimeException(strtr("%binary not executable.", ['%binary' => $binary]));
    }
    return TRUE;
  }

  /**
   * Discover the executable.
   *
   * @param string $tool
   *   The tool to discover.
   *
   * @return string|null
   *   The path to the binary or NULL if not found.
   */
  public function discoverExecutable(string $tool) {
    try {
      $command = strtr("which %tool", ['%tool' => $tool]);
      $process = Process::fromShellCommandline($command);
      $process->run();
      if (!$process->isSuccessful()) {
        return;
      }
      $binary = trim($process->getOutput());
      if (!empty($binary)) {
        return $this->validateExecutable($binary) ? $binary : NULL;
      }
      return;
    }
    catch (\Exception $e) {
      $this->logger->warning($e->getMessage());
      return;
    }
  }

  /**
   * Get the filename.
   *
   * @param string $suffix
   *   The filename suffix.
   *
   * @return string
   *   The filename.
   */
  public function filename(string $suffix = '') {
    $siteName = str_replace(' ', '', strtolower($this->configFactory->get('system.site')->get('name')));
    if (empty($siteName)) {
      $siteName = 'drupal';
    }
    // Transliterate the name.
    $siteName = $this->transliteration->transliterate($siteName, 'en');
    return strtr('sbom-%site-%format-%id%suffix.json', [
      '%site' => $siteName,
      '%format' => $this->configFactory->get('drubom.settings')->get('sbom_output_format') ?? 'cyclonedx-json',
      '%id' => uniqid(),
      '%suffix' => !empty($suffix) ? '-$suffix' : '',
    ]);
  }

  /**
   * Execute a command.
   *
   * @param string $command
   *   The command to execute.
   * @param array $options
   *   The options.
   *
   * @return \Symfony\Component\Process\Process
   *   The process.
   *
   * @throws \InvalidArgumentException
   *   If the command is empty.
   * @throws \Symfony\Component\Process\Exception\ProcessFailedException
   *   If the process fails.
   */
  public function execute(string $command, array $options = []) {
    if (empty($command)) {
      throw new \InvalidArgumentException('Command cannot be empty.');
    }
    if (empty($root)) {
      $root = $this->appRoot;
    }
    $default_options = [
      'root'    => $this->appRoot,
      'timeout' => self::PROCESS_TIMEOUT,
    ];
    $options = array_merge($default_options, $options);

    // Run the command and save file.
    $this->logger->info('Running command:' . $command);
    $process = Process::fromShellCommandline($command, $options['root']);
    $process->setTimeout($options['timeout']);
    $process->run();
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
    }
    return $process;
  }

}

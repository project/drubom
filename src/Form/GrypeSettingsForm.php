<?php

namespace Drupal\drubom\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\drubom\DrubomExecManager;
use Drupal\drubom\GrypeController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure drubom settings for this site.
 */
final class GrypeSettingsForm extends ConfigFormBase {

  /**
   * The DrubomExecManager service.
   *
   * @var \Drupal\drubom\DrubomExecManager
   * @var \Drupal\drubom\GrypeController
   */
  public function __construct(
        private readonly DrubomExecManager $drubomExecManager,
        private readonly GrypeController $grypeController
    ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
          $container->get('drubom.exec_manager'),
          $container->get('drubom.grype_controller')

      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drubom_grype_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drubom.settings.grype'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to binary'),
      '#description' => $this->t('The path to the grype binary, if empty will try to auto-detect it. You can download it from <a target="" href=":url">here</a>.', [':url' => 'https://github.com/anchore/grype/releases']),
      '#default_value' => $this->config('drubom.settings.grype')->get('path')
        ? $this->config('drubom.settings.grype')->get('path')
        : $this->drubomExecManager->discoverExecutable('grype'),
    ];
    $form['db_container'] = [
      '#type' => 'fieldset',
    ];

    $form['db_container']['latest_update'] = [
      '#type' => 'markup',
      '#prefix' => '<div id="db-latest-update">',
      '#markup' => $this->t(
              'Database latest update: %date
                <p> <small> You can update it using the following drush command: <code> drush drubom:vuln-updb </code> </small> </p>',
              ['%date' => $this->grypeController->getLatestUpdate() ?? 'Never']
      ),
      '#postfix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $binary = $form_state->getValue('path');
    if (!file_exists($binary)) {
      $form_state->setErrorByName($binary, $this->t('"%binary" binary not found, please check the path, it must be accessible from Drupal', ["%binary" => $binary]));
    }
    if (!is_executable($binary)) {
      $form_state->setErrorByName($binary, $this->t('"%binary" binary found but is not executable.', ["%binary" => $binary]));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('drubom.settings.grype')
      ->set('path', $form_state->getValue('path'))
      ->set('db_refresh', $form_state->getValue('db_refresh'))
      ->set('db_path', $form_state->getValue('db_path'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

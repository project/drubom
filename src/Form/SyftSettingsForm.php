<?php

namespace Drupal\drubom\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\drubom\DrubomExecManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure drubom settings for this site.
 */
final class SyftSettingsForm extends ConfigFormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The Link Generator service.
   * @param \Drupal\drubom\DrubomExecManager $drubomExecManager
   *   Drubom execManager.
   */
  final public function __construct(
        ConfigFactoryInterface $configFactory,
        TypedConfigManagerInterface $typedConfigManager,
        LinkGeneratorInterface $link_generator,
        private readonly DrubomExecManager $drubomExecManager
    ) {
    parent::__construct($configFactory, $typedConfigManager);
    $this->link = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
          $container->get('config.factory'),
          $container->get('config.typed'),
          $container->get('link_generator'),
          $container->get('drubom.exec_manager'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drubom_syft_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drubom.settings.syft'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@syft binary executable file', [
        '@syft' => $this->link->generate($this->t('Syft'), Url::fromUri('https://github.com/anchore/syft', [
          'absolute' => TRUE,
          'attributes' => ['target' => 'blank'],
        ])),
      ]),
      '#placeholder' => '/var/www/html/syft',
      '#description' => $this->t('The path to the Syft binary executable
              file, including the file name itself at the end (if empty the module will try to
              auto-detect it). <br>You can download the latest release of Syft from @syft_releases.
              <br><u>Hint for @ddev users</u>: assuming you working on a Linux server container,
              you could download the latest Syft release for that OS
              (i.e.: @syft_latest_release)<br>and eventually store in the root of your Drupal
              code base (where your composer.json is located), ending up in the following
              setting: "/var/www/html/syft".', [
                '@syft_releases' => $this->link->generate($this->t('here'), Url::fromUri('https://github.com/anchore/syft/releases', [
                  'absolute' => TRUE,
                  'attributes' => ['target' => 'blank'],
                ])),
                '@ddev' => $this->link->generate($this->t('DDEV'), Url::fromUri('https://ddev.readthedocs.io/en/stable/', [
                  'absolute' => TRUE,
                  'attributes' => ['target' => 'blank'],
                ])),
                '@syft_latest_release' => $this->link->generate($this->t('syft_0.102.0_linux_amd64.tar.gz'), Url::fromUri('https://github.com/anchore/syft/releases/download/v0.102.0/syft_0.102.0_linux_amd64.tar.gz', [
                  'absolute' => TRUE,
                  'attributes' => ['target' => 'blank'],
                ])),
              ]),
      '#default_value' => $this->config('drubom.settings.syft')->get('path')
        ? $this->config('drubom.settings.syft')->get('path')
        : $this->drubomExecManager->discoverExecutable('syft'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      $binary = $form_state->getValue("path");
      if (empty($binary)) {
        return;
      }
      $this->drubomExecManager->validateExecutable($binary);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName($binary, $e->getMessage());
    } finally {
      parent::validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('drubom.settings.syft')
      ->set('path', $form_state->getValue('path'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

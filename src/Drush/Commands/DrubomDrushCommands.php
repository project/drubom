<?php

namespace Drupal\drubom\Drush\Commands;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\State\StateInterface;
use Drupal\drubom\Grype\GrypeState;
use Drupal\drubom\GrypeController;
use Drupal\drubom\SyftController;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Symfony\Component\Console\Helper\Table;

/**
 * Provides drush command to generate sbom.
 */
class DrubomDrushCommands extends DrushCommands {

  /**
   * Constructor.
   *
   * @param \Drupal\drubom\SyftController $syftController
   *   The syft controller.
   * @param \Drupal\drubom\GrypeController $grypeController
   *   The grype controller.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   * @param \Drupal\drubom\Grype\GrypeState $grypeState
   *   The grype state service.
   */
  public function __construct(
    private readonly SyftController $syftController,
    private readonly GrypeController $grypeController,
    private readonly StateInterface $state,
    private readonly DateFormatter $dateFormatter,
    private readonly GrypeState $grypeState
  ) {
    parent::__construct();
  }

  /**
   * Log a sanitized error.
   *
   * @param string $error
   *   The error message.
   */
  private function logError($error) {
    $error = preg_replace("/\033\[[^m]*m/", '', $error);
    $this->logger()->error($error);
  }

  /**
   * Print Syft binary information.
   *
   * @command drubom:syft-info
   */
  public function syftInfo() {
    try {
      $syftInfo = (array) $this->syftController->getSyftFullVersion();
      foreach ($syftInfo as $key => $value) {
        $rows[] = [
          'key' => $key,
          'value' => $value,
        ];
      }
      $table = new Table($this->output);
      $table->setHeaders(['Key', 'Value']);
      $table->setRows($rows);
      return $table->render();
    }
    catch (\Exception $e) {
      throw new \Exception(dt($e->getMessage()));
    }
  }

  /**
   * Print Grype binary information.
   *
   * @command drubom:grype-info
   */
  public function grypeInfo() {
    try {
      $syftInfo = (array) $this->grypeController->getFullVersion();
      foreach ($syftInfo as $key => $value) {
        $rows[] = [
          'key' => $key,
          'value' => $value,
        ];
      }
      $table = new Table($this->output);
      $table->setHeaders(['Key', 'Value']);
      $table->setRows($rows);
      return $table->render();
    }
    catch (\Exception $e) {
      $this->logError($e->getMessage());
    }
  }

  /**
   * Generates the SBOM.
   *
   * @command drubom:sbom-generate
   */
  public function generateSbom() {
    try {
      $syftVersion = $this->syftController->getSyftFullVersion()->version;
      $this->logger()->notice(dt('Generating SBOM with Syft @version...', ['@version' => $syftVersion]));
      $this->syftController->run();
      $this->logger()->success(dt('SBOM generated and saved.'));
    }
    catch (\Exception $e) {
      throw new \Exception(dt($e->getMessage()));
    }
  }

  /**
   * Download SBOM.
   *
   * @command drubom:sbom-download
   */
  public function downloadSbom() {
    $this->logger()->notice(dt('Downloading SBOM...'));
    $drubom = $this->state->get('drubom.sbom');
    if (!empty($drubom['data'])) {
      return $drubom['data'];
    }
    $this->logger()->error('SBOM not yet generated.');
  }

  /**
   * Print vulnerability database info.
   *
   * @command drubom:vuln-db-info
   */
  public function getVulnerabilityDbInfo() {
    try {
      $this->logger()->notice(dt('Vulnerability database info...'));
      if ($this->grypeState->isEmpty()) {
        $this->logger()->warning(dt('Database not found.'));
        return 0;
      }
      $metadata = $this->grypeState->getMetadata();
      $metadataTable = new Table($this->output);
      $metadataTable->setHeaders(array_keys($metadata));
      $metadataTable->setRows([$metadata]);

      // Print files.
      $filesTable = new Table($this->output);
      $files = $this->grypeState->getFiles();
      $rows = [];
      foreach ($files as $file) {
        $rows[] = $file->getRow();
      }
      $filesTable->setHeaders(array_keys($rows[0]));
      $filesTable->setRows($rows);
      return $metadataTable->render() . $filesTable->render();
    }
    catch (\Exception $e) {
      $this->logger()->error($e->getMessage());
      $this->logger()->error(dt('State is not valid. Please update the database with `drush drubom:vuln-updb`.'));
    }
  }

  /**
   * Update the vulnerability database.
   *
   * @command drubom:vuln-db-update
   */
  public function updateVulnDatabase() {
    $date = $this->grypeController->getLatestUpdate();
    $this->logger()->notice(dt('Last update: @update', [
      '@update' => empty($date)
        ? 'never'
        : $this->dateFormatter->format(strtotime($date), 'custom', 'r'),
    ]));
    if (!$this->io()->confirm(dt('Do you want to update ? It may take a while to download the database.'))) {
      throw new UserAbortException();
    }
    try {
      $this->logger()->notice(dt('Updating database it may take a while...'));
      $this->grypeState->clear();
      $this->grypeController->updateDatabase($this->grypeState);
      $files = $this->grypeState->getFiles();
      if (empty($files)) {
        $this->logger()->error(dt('No files saved, something went wrong.'));
        return;
      }
      $this->logger()->notice(dt('Database successfully updated, @count files saved.', ['@count' => count($files)]));
      return $this->getVulnerabilityDbInfo();
    }
    catch (\Exception $e) {
      $this->logError($e->getMessage());
    }
  }

  /**
   * Clear the vulnerability database.
   *
   * @command drubom:vuln-db-clear
   */
  public function clearVulnDatabase() {
    if (!$this->io()->confirm(dt('Do you want to clear the database ?'))) {
      throw new UserAbortException();
    }
    try {
      $this->logger()->notice(dt('Clearing database...'));
      $this->grypeState->clear();
      $this->logger()->success(dt('Database cleared.'));
    }
    catch (\Exception $e) {
      $this->logError($e->getMessage());
    }
  }

  /**
   * Run the vulnerability scan.
   *
   * @command drubom:vuln-scan
   */
  public function runVulnScan() {
    try {
      $this->logger()->notice(
        dt(
          'Running vulnerability scan with Grype @version it may take a while...',
          ['@version' => $this->grypeController->getFullVersion()->version]
        )
      );
      $this->grypeController->run();
      $this->logger()->success(dt('Vulnerability scan completed and SBOM saved'));
    }
    catch (\Exception $e) {
      $this->logError($e->getMessage());
    }
  }

  /**
   * Download Vulnerability SBOM.
   *
   * @command drubom:vuln-sbom-download
   */
  public function downloadVulnSbom() {
    $this->logger()->notice(dt('Downloading vulnerability SBOM...'));
    $sbom = $this->grypeController->downloadSbom();
    if (!empty($sbom)) {
      return $sbom;
    }
    $this->logger()->error('SBOM not yet generated.');
  }

}

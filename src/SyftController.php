<?php

namespace Drupal\drubom;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\State;
use Psr\Log\LoggerInterface;

/**
 * Syft controller.
 */
class SyftController {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\State\State $state
   *   The state service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\drubom\DrubomExecManager $drubomExecManager
   *   The DrubomExecManager service.
   * @param string $appRoot
   *   The application root.
   *
   * @return void
   */
  public function __construct(
        private readonly State $state,
        private readonly FileSystemInterface $fileSystem,
        private readonly ConfigFactoryInterface $configFactory,
        private readonly LoggerInterface $logger,
        private readonly DrubomExecManager $drubomExecManager,
        private readonly string $appRoot,
    ) {
  }

  /**
   * Find the syft binary. If not configured, try to discover it.
   *
   * @return string|Exception
   *   The path to the binary or an Exception if not found.
   */
  public function getSyftBinary() {
    $binary = $this->configFactory->get('drubom.settings.syft')->get('path');
    if (empty($binary)) {
      $binary = $this->drubomExecManager->discoverExecutable('syft');
    }

    return $binary;
  }

  /**
   * Get the full version of the syft binary.
   *
   * @return string
   *   The full version of the syft binary.
   *
   * @throws \RuntimeException
   *   If the binary is not found or not configured.
   */
  public function getSyftFullVersion() {
    $binary = $this->getSyftBinary();
    if (empty($binary)) {
      throw new \RuntimeException('Syft binary not found or not configured, cannot get help.');
    }
    $command = strtr('%binary version -o json', ['%binary' => $binary]);
    $process = $this->drubomExecManager->execute($command);
    if ($process->isSuccessful()) {
      $json = json_decode($process->getOutput());
      json_last_error() === JSON_ERROR_NONE || throw new \RuntimeException('Invalid JSON: ' . json_last_error_msg());
      return $json;
    }

    throw new \RuntimeException($process->getErrorOutput());
  }

  /**
   * Run the syft binary.
   *
   * @param array $options
   *   The options to pass to the binary.
   *
   * @return string
   *   The SBOM.
   *
   * @throws \RuntimeException
   *   If the binary is not found or not configured.
   */
  public function run(array $options = []) {
    try {
      $default_options = [
        'saveToState' => TRUE,
      ];
      $options = array_merge($default_options, $options);
      $binary = $this->getSyftBinary();
      if (empty($binary)) {
        throw new \RuntimeException('Syft binary not found or not configured, cannot generate SBOM.');
      }

      // Create a temporary file to store the SBOM.
      $sbomFile = strtr('%tmpDir/%file', [
        '%tmpDir' => $this->fileSystem->getTempDirectory(),
        '%file' => $this->drubomExecManager->filename(),
      ]);

      // Create the command line.
      $format = $this->configFactory->get('drubom.settings')->get('sbom_output_format') ?? 'cyclonedx-json';
      $command = strtr('%syft_binary -o %format --file "%file" %root', [
        '%syft_binary' => $binary,
        '%file' => $sbomFile,
        '%root' => dirname($this->appRoot),
        '%format' => $format,
      ]);
      $this->drubomExecManager->execute($command);

      $sbom = file_get_contents($sbomFile);
      if ($options['saveToState'] === FALSE) {
        return $sbom;
      }
      // Save to state.
      $sbom_stored = [
        'timestamp' => time(),
        'format' => $format,
        'data' => $sbom,
      ];
      $this->state->set('drubom.sbom', $sbom_stored);

      return $sbom;
    }
    catch (\Exception $e) {
      throw $e;
    } finally {
      if (!empty($sbomFile) && file_exists($sbomFile)) {
        $this->fileSystem->delete($sbomFile);
      }
    }
  }

}

<?php

namespace Drupal\drubom\Grype;

use Drupal\Core\StringTranslation\ByteSizeMarkup;

/**
 * Represents a file in the Grype state.
 */
class GrypeFile {
  /**
   * The filename.
   *
   * @var string
   */
  private string $filename;

  /**
   * The size of the file.
   *
   * @var string
   */
  private string $size;

  /**
   * The path to the file in the Drupal filesystem.
   *
   * @var string
   */
  private string $drupalPath;

  /**
   * Constructs a new GrypeFile object.
   *
   * @param mixed $file
   *   The file object.
   * @param string $drupalPath
   *   The path to the file in the Drupal filesystem.
   */
  public function __construct(mixed $file, string $drupalPath) {
    $this->drupalPath = $drupalPath;
    $this->filename = $file->filename;
    $this->size = (int) filesize($file->uri);
  }

  /**
   * Delete the file.
   */
  public function delete() {
    unlink($this->drupalPath);
  }

  /**
   * Get the properties of the object.
   *
   * @return array
   *   The properties of the object.
   */
  public function getProps(): array {
    return array_keys(get_object_vars($this));
  }

  /**
   * Get the row for the table.
   *
   * @return array
   *   The row for the table.
   */
  public function getRow(): array {
    return [
      'path' => $this->getDrupalPath(),
      'size' => $this->getSize(),
    ];
  }

  /**
   * Get the filename.
   *
   * @return string
   *   The filename.
   */
  public function getFilename(): string {
    return $this->filename;
  }

  /**
   * Get the size.
   *
   * @return string
   *   The file size formatted.
   */
  public function getSize(): string {
    return ByteSizeMarkup::create($this->size);
  }

  /**
   * Get the path to the file in the Drupal filesystem.
   *
   * @return string
   *   The path to the file in the Drupal filesystem.
   */
  public function getDrupalPath(): string {
    return $this->drupalPath;
  }

}

<?php

namespace Drupal\drubom;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;
use Drupal\drubom\Grype\GrypeState;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Runs the syft binary.
 */
class GrypeController {
  const GRYPE_DB_PATH = 'private://drubom/grype-db';

  /**
   * Constructs a new GrypeController object.
   *
   * @param \Drupal\Core\State\State $state
   *   The state service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\drubom\DrubomExecManager $drubomExecManager
   *   The DrubomExecManager service.
   * @param \Drupal\drubom\Grype\GrypeState $grypeState
   *   The GrypeState service.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger service.
   * @param string $appRoot
   *   The app root.
   */
  public function __construct(
        private readonly State $state,
        private readonly FileSystemInterface $fileSystem,
        private readonly ConfigFactoryInterface $configFactory,
        private readonly LoggerInterface $logger,
        private readonly DrubomExecManager $drubomExecManager,
        private readonly GrypeState $grypeState,
        private readonly Messenger $messenger,
        private readonly string $appRoot,
    ) {
  }

  /**
   * Find the grype binary. If not configured, try to discover it.
   *
   * @return string|Exception
   *   The path to the binary or an Exception if not found.
   */
  public function getBinary() {
    $binary = $this->configFactory->get('drubom.settings.grype')->get('path');
    if (empty($binary)) {
      $binary = $this->drubomExecManager->discoverExecutable('grype');
    }
    if (empty($binary) || !file_exists($binary)) {
      throw new \Exception('Grype binary not found or not configured.');
    }

    return $binary;
  }

  /**
   * Save files and metadata to the drupal filesystem and state.
   *
   * @param string $tmpDir
   *   The temporary directory.
   * @param \Drupal\drubom\Grype\GrypeState $grypeState
   *   The GrypeState service.
   */
  private function persistDatabase(string $tmpDir, GrypeState $grypeState) {
    if (empty($tmpDir)) {
      throw new \Error('Cannot read files from an empty directory.');
    }
    $directory = self::GRYPE_DB_PATH;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $dbFiles = $this->fileSystem->scanDirectory($tmpDir, '/.*\.(db|json)$/');
    if (empty($dbFiles)) {
      throw new \Error('Cannot find any database file in ' . $tmpDir);
    }
    foreach ($dbFiles as $file) {
      $dest = $directory . '/' . $file->filename;
      $this->fileSystem->copy($file->uri, $dest, FileSystemInterface::EXISTS_REPLACE);
      $grypeState->addFile($file, $dest);
      if ($file->filename === 'metadata.json') {
        $grypeState->setMetadata($file);
      }
    }
    $grypeState->drupalCommit();
  }

  /**
   * Update the grype database.
   */
  public function updateDatabase($grypeState) {
    $tmpDir = '';
    try {
      $binary = $this->getBinary();
      $destDir = $this->fileSystem->getTempDirectory() . DIRECTORY_SEPARATOR . uniqid('drubom-grype-db-');
      $command = strtr('GRYPE_DB_CACHE_DIR=%tmpDir %grype db update', [
        '%tmpDir' => $destDir,
        '%grype' => $binary,
      ]);
      $this->drubomExecManager->execute($command, ['timeout' => 1800]);
      $this->persistDatabase($destDir, $grypeState);
    }
    catch (ProcessFailedException $e) {
      $process = $e->getProcess();
      $errorOutput = $process->getErrorOutput();
      $this->logger->error($errorOutput);
      throw new \Exception($errorOutput);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      throw new \Exception($e->getMessage());
    } finally {
      if (!empty($tmpDir) && file_exists($tmpDir)) {
        $this->fileSystem->deleteRecursive($tmpDir);
      }
    }
  }

  /**
   * Get db last update from metadata.
   *
   * @return string|null
   *   The last update date or null if an error occurs.
   */
  public function getLatestUpdate() {
    try {
      return $this->grypeState->getMetadata()['built'];
    }
    catch (\Exception) {
      return;
    }
  }

  /**
   * Get the full version of the grype binary.
   *
   * @return object|Exception
   *   The full version or an Exception if an error occurs.
   */
  public function getFullVersion() {
    $binary = $this->getBinary();
    $command = strtr('%binary version -o json', ['%binary' => $binary]);
    $process = $this->drubomExecManager->execute($command);
    $json = json_decode($process->getOutput());
    json_last_error() === JSON_ERROR_NONE || throw new \RuntimeException('Invalid JSON: ' . json_last_error_msg());

    return $json;
  }

  /**
   * Prepare the database files.
   *
   * @return string
   *   The path to the database files.
   */
  private function prepareDbFiles(): string {
    $path = $this->grypeState->getCachedCopyFilesPath();
    if (!empty($path) && getenv('DRUBOM_GRYPE_DB_PATH') === $path) {
      $this->logger->notice('Using cached files from: $path');
      return $path;
    }

    // Set the destination directory.
    $root = strtr('%tmpDir', [
      '%tmpDir' => !empty(getenv('DRUBOM_GRYPE_DB_PATH'))
        ? getenv('DRUBOM_GRYPE_DB_PATH')
        : $this->fileSystem->getTempDirectory() . DIRECTORY_SEPARATOR . uniqid('drubom-grype-db-'),
    ]);
    $destDir = strtr('%root/%version', [
      '%root' => $root,
      '%version' => $this->grypeState->getMetadata()['version'],
    ]);

    $this->fileSystem->prepareDirectory($destDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $files = $this->grypeState->getFiles();
    foreach ($files as $file) {
      $this->fileSystem->copy($file->getDrupalPath(), $destDir . '/' . $file->getFilename(), FileSystemInterface::EXISTS_REPLACE);
    }
    $this->grypeState->setCachedCopyFilesPath($root);

    return $root;
  }

  /**
   * Prepare command.
   *
   * @param string $destDir
   *   The destination directory.
   * @param string $destFile
   *   The destination file.
   *
   * @return string
   *   The command to run.
   */
  private function prepareCommand($destDir, $destFile): string {
    $binary = $this->getBinary();
    $envVars = strtr('GRYPE_DB_AUTO_UPDATE=false GRYPE_DB_VALIDATE_AGE=false GRYPE_DB_CACHE_DIR=%tmpDir', [
      '%tmpDir' => $destDir,
    ]);
    $command = strtr(
      '$envVars %binary -o cyclonedx-json --file %destFile %root',
          [
            '%binary' => $binary,
            '%destFile' => $destFile,
            '%root' => dirname($this->appRoot),
          ]
      );

    return $command;
  }

  /**
   * Download the SBOM.
   */
  public function downloadSbom() {
    $drubom = $this->grypeState->getSbomContainer();
    if (!empty($drubom['data'])) {
      return $drubom['data'];
    }
    $this->logger->error('Vulnerability SBOM not yet generated.');
  }

  /**
   * Run the grype binary.
   *
   * @return array|Exception
   *   The SBOM or an Exception if an error occurs.
   */
  public function run() {
    $destFile = '';
    try {
      if ($this->grypeState->isEmpty()) {
        throw new \Exception('Grype database not found. Please update the database.');
      }
      $destDir = $this->prepareDbFiles();
      $destFile = strtr('%tmpDir/%file', [
        '%tmpDir' => $this->fileSystem->getTempDirectory(),
        '%file' => $this->drubomExecManager->filename(),
      ]);
      $command = $this->prepareCommand($destDir, $destFile);
      $this->drubomExecManager->execute($command);

      // Save the file to state.
      $data = file_get_contents($destFile);
      $sbom = [
        'timestamp' => time(),
        'format' => 'cyclonedx-json',
        'data' => $data,
      ];

      return $this->grypeState->saveSbom($sbom);
    }
    catch (\Exception $e) {
      throw $e;
    } finally {
      if (!empty($destFile) && file_exists($destFile)) {
        $this->fileSystem->delete($destFile);
      }
    }
  }

}

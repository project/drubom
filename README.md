# DruBOM

## Comprehensive SBOM and Vulnerability Reporting for Drupal

DruBOM enriches Drupal by seamlessly integrating with [Anchore Syft](https://github.com/anchore/syft) and [Anchore Grype](https://github.com/anchore/grype) to deliver detailed Software Bills of Materials (SBOMs) and vulnerability assessments. This module captures PHP dependencies and any additional packages recognized by Syft and Grype, offering a thorough insight into your Drupal site's software inventory and security posture.

### Prerequisites

- Supports Drupal version 10.2.x and upwards.
-
- Module installation and updates must be performed via Composer to ensure all dependencies are correctly managed. Use the following command to install:

```shell
  composer require drupal/drubom
```

> For detailed instructions, see [Download contributed modules and themes using Composer](https://www.drupal.org/node/2718229#adding-modules).

- [Anchore Syft](https://github.com/anchore/syft), essential for generating SBOMs, is available as a CLI tool and Go library. For Linux server containers, download the Syft executable suitable for your environment from the [latest releases](https://github.com/anchore/syft/releases).

- [Anchore Grype](https://github.com/anchore/syft), optional and needed for generating Vulnerabilities SBOMs, is available as a CLI tool and Go library. For Linux server containers, download the Syft executable suitable for your environment from the [latest releases](https://github.com/anchore/grype/releases).

- PHP configuration must permit the use of `proc_open()`, maintaining the same security level as `exec()`.

### Setup and Usage

Activate the module via Drush with:

```shell
drush en drubom
```

Alternatively, enable it through the Drupal backend.

- Visit the module's settings page (admin/config/system/drubom) to select the SBOM format (CycloneDX JSON or Spdx JSON).

#### Generating SBOMs

- Specify the [Syft binary executable's](https://github.com/anchore/syft) path in the module's settings (admin/config/system/drubom/syft).

- Generate or download SBOM reports from the DruBOM Report section (admin/reports/sbom) by using the "Generate" or "Download" options.

#### Vulnerability Assessments

- Designate the [Grype executable's](https://github.com/anchore/grype) location in the module's settings (admin/config/system/drubom/grype).

- Update the Grype vulnerability database via Drush: `drubom:vuln-db-update`

- Initiate a vulnerability scan with: `drubom:vuln-scan`

- Obtain the vulnerability SBOM through Drush: `drubom:vulm-sbom-download`

#### Drush Commands

DruBOM is equipped with a variety of Drush commands for streamlined module interaction:

```shell
  drubom:grype-info          - Display Grype binary details.
  drubom:sbom-download       - Download SBOM.
  drubom:sbom-generate       - Generate an SBOM.
  drubom:syft-info           - Show Syft binary details.
  drubom:vuln-sbom-download  - Retrieve Vulnerability SBOM.
  drubom:vuln-db-clear       - Clear the vulnerability database.
  drubom:vuln-db-info        - Display vulnerability database information.
  drubom:vuln-db-update      - Update the vulnerability database.
  drubom:vuln-scan           - Conduct a vulnerability scan.
```

### Acknowledgments

- **Paolo Mainardi** - [paolomainardi](https://www.drupal.org/u/paolomainardi) (Founder)
- **Luca Lusso** - [lussoluca](https://www.drupal.org/u/lussoluca)
- **Eduardo Dusi** - [edodusi](https://www.drupal.org/u/edodusi)
- **Blade Du** - [bladedu](https://www.drupal.org/u/bladedu)
- **Itamar Ir** - [itamair](https://www.drupal.org/u/itamair)

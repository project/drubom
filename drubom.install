<?php

/**
 * @file
 * Install, update and uninstall functions for the drubom module.
 */

use Drupal\Core\Url;

/**
 * Implements hook_requirements().
 */
function drubom_requirements($phase) {
  $requirements = [];

  if ($phase === 'runtime') {
    $syft_binary = \Drupal::service('drubom.syft_controller')->getSyftBinary();

    if (is_null($syft_binary)) {
      $requirements['drubom_syft']['title'] ='DruBOM';
      $requirements['drubom_syft']['description'] = t('Syft binary not found or not configured.<br>Please configure the path to the Syft binary in the <a href=":url">settings page</a>.',
        [':url' => Url::fromRoute('drubom.settings_form')->toString()]);
      $requirements['drubom_syft']['severity'] = REQUIREMENT_WARNING;
    }

    $requirements['drubom']['title'] ='DruBOM';
    $requirements['drubom']['severity'] = REQUIREMENT_INFO;

    $state = \Drupal::state();
    $drubom = $state->get('drubom.sbom');

    if (!empty($drubom['timestamp'])) {
      $requirements['drubom']['description'] = t('SBOM generated on: %date. See it on the <a href=":url">report page</a>.',
        [
          '%date' => date('Y-m-d - H:i:s', $drubom['timestamp']),
          ':url' => Url::fromRoute('drubom.report')->toString()
        ]);
    }
    else {
      $requirements['drubom']['description'] = t('SBOM not yet generated. Generate it on the <a href=":url">report page</a>.',
          [':url' => Url::fromRoute('drubom.report')->toString()]);
    }
  }

  return $requirements;
}
